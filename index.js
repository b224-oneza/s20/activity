// alert("Hello World!")

for(let number = Number(prompt("Give me a number: ")); number >= 0; number--) {
	if(number <= 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break	
	}
	if(number % 10 === 0) { 
		console.log("The number is divisible by 10. Skipping the number.");
		continue
	}
	if(number % 5 === 0) {
		console.log(number);
	}
}

let longestWordInDictionary = "supercalifragilisticexpialidocious";
console.log(longestWordInDictionary);

let consonants = " ";


for(let n = 0; n < longestWordInDictionary.length; n++){
	if(
		longestWordInDictionary[n].toLowerCase() == "a" ||
		longestWordInDictionary[n].toLowerCase() == "e" ||
		longestWordInDictionary[n].toLowerCase() == "i" ||
		longestWordInDictionary[n].toLowerCase() == "o" ||
		longestWordInDictionary[n].toLowerCase() == "u" 
	) {
		continue;
	} else {
		consonants += longestWordInDictionary[n];
	};
}; 
console.log(consonants)